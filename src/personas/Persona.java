package personas;

/**
 *
 * @author Rubén Martínez Cabello ad@martinezad.es
 * 
**/
import java.util.Date; 

public class Persona 
{
    // Atributos solicitados
    private String  nombre;
    private Integer edad;
    private String  DNI;
    private String  sexo;
    private Float   peso;
    private Float   altura;
    // Atributos adicionales
//    private Date    fNacimiento;
    private Boolean trabaja;

    // Getters y setters
    public String setNombre(String nombre) { if (!nombre.isBlank()) this.nombre = nombre; return this.nombre; }
    public String getNombre() { return this.nombre; }
    
    public Integer setEdad(Integer edad)
    {
        if (edad >=0)
        {
            this.edad = edad;
        };
        return this.edad;
    }
    public Integer getEdad() { return this.edad; }
    
    //public String setDNI() NO DEFINIDO
    public String getDNI() { return this.DNI; }
    
    public String setSexo(String sexo) { if ( sexo.equals(Persona.SEXO_HOMBRE) || sexo.equals(Persona.SEXO_MUJER) ) { this.sexo = sexo; }; return this.sexo; }
    public String getSexo() { return this.sexo; }

    public Float setPeso(Float peso) { if (peso >= Persona.default_peso) { this.peso = peso; }; return this.peso; }
    public Float getPeso() { return this.peso; }
    
    public Float setAltura(Float altura) { if (altura >= Persona.default_altura) { this.altura = altura; }; return this.altura; }
    public Float getAltura() { return this.altura; }
    
    public Boolean setTrabaja(Boolean trabaja) { return (this.trabaja = trabaja); }
    public Boolean getTrabaja() { return this.trabaja; }
    
    
    
    
    // Valores por defecto
    public static final String  default_nombre = "";
    public static final Integer default_edad = 0;
    public static final String  SEXO_HOMBRE = "H";
    public static final String  SEXO_MUJER = "M";
    public static final String  default_sexo = Persona.SEXO_HOMBRE;
    public static final Float   default_peso = 0.0f;
    public static final Float   default_altura = 0.0f;
    //public static final Date    default_fNacimiento = new Date(0L); 
    public static final Boolean default_trabaja = false; 
    public static final Integer PESO_IDEAL = 0;
    public static final Integer INFRAPESO = -1;
    public static final Integer SOBREPESO = 1;
    

    
    Persona()
    {
        this.nombre = Persona.default_nombre;
        this.DNI = this.GenerarDNI();
        this.edad = Persona.default_edad;
        this.sexo = Persona.default_sexo;
        this.peso = Persona.default_peso;
        this.altura = Persona.default_altura;
        //this.fNacimiento = Persona.default_fNacimiento; 
        this.trabaja = Persona.default_trabaja;
    }

    
    Persona(String nombre, Integer edad, String sexo, Float peso, Float altura, Boolean trabaja)
    {
        this(); // Llamada al constructor vacio
        this.setNombre( nombre );
        this.setEdad( edad );
        this.setSexo( sexo );
        this.setPeso( peso );
        this.setAltura( altura );
        this.setTrabaja( trabaja );
    }
    
    Persona(String nombre, Integer edad, String sexo)
    {
        this(); // Llamada al constructor vacio
        this.setNombre( nombre );
        this.setEdad( edad );
        this.setSexo( sexo );
    }
    
    private String GenerarDNI()
    {
        Integer num = 1 + ((Double)(Math.random() * 99999999)).intValue();
        return String.format("%08d", num) + Persona.CalcularLetraDNI(num);
    }
    public static String CalcularLetraDNI(Integer n)
    {
        return "TRWAGMYFPDXBNJZSQVHLCKE".substring( (n%23), (n%23) + 1);
    }
    
    public Integer calcularIMC()
    {
        Double imc = ( (double)(this.getPeso()) ) / Math.pow( (double)(this.getAltura()), 2.0);
        if (imc<20) 
            return Persona.INFRAPESO;
        else if (imc>25) 
            return Persona.SOBREPESO;
        else 
            return Persona.PESO_IDEAL;
    }
    
    public Boolean esMayorDeEdad()
    {
        return (this.getEdad()>=18);
    }
    
    private Boolean comprobarSexo(String sexo)
    {
        return sexo.equals(Persona.SEXO_HOMBRE) || sexo.equals(Persona.SEXO_MUJER);
    }
    
    public String toString()
    {
        String acc = "";
        acc += "Nombre:" + this.getNombre() + System.lineSeparator();
        acc += "DNI   :" + this.getDNI() + System.lineSeparator();
        acc += "Edad  :" + this.getEdad().toString() + " - Es "  + ((this.esMayorDeEdad()) ? "mayor" : "menor") + " de edad"  + System.lineSeparator();
        acc += "Empleo:" + ((this.getTrabaja()) ? "SI" : "NO") + System.lineSeparator();
        
        acc += "Altura:" + this.getAltura().toString() + "m." + System.lineSeparator();
        
        acc += "Peso  :" + this.getPeso().toString() + "Kg. ";
        switch (this.calcularIMC()) {
            case -1:
                acc += "INFRAPESO" ;
                break;
            case +0:
                acc += "Peso ideal";
                break;
            case +1:
                acc += "SOBREPESO" ;
                break;
        };
        acc += System.lineSeparator();
        return acc;
    }

}
