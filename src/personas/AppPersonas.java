package personas;

import javax.swing.JOptionPane;

/**
 *
 * @author Rubén Martínez Cabello ad@martinezad.es
 * 
**/
public class AppPersonas 
{

    public static void main(String[] args) 
    {
        String s, nombre, sexo;
        Integer edad;
        Float peso, altura;
        Persona p1, p2, p3;
        
        s = JOptionPane.showInputDialog("Introduce el nombre:");
        nombre = s;
        
        s = JOptionPane.showInputDialog("Introduce la edad en años (ej: 22):");
        edad = Integer.parseInt(s);
        
        s = JOptionPane.showInputDialog("Introduce el sexo (ej: H(ombre) ó M(ujer)):");
        sexo = s.toUpperCase().substring(0, 1);

        s = JOptionPane.showInputDialog("Introduce la altura en metros (ej: 1.84):");
        altura = Float.parseFloat(s);
        
        s = JOptionPane.showInputDialog("Introduce el peso en kilogramos (ej: 84.5):");
        peso = Float.parseFloat(s);
        
        System.out.println("Creando personas [p1, p2, p3]");
        p1 = new Persona(nombre, edad, sexo, peso, altura, Persona.default_trabaja);
        p2 = new Persona(nombre, edad, sexo);
        p3 = new Persona();
        
        System.out.println("----------------------------------------------------------------");
        
        System.out.print(" p1 está ");
        switch (p1.calcularIMC()) {
            case -1: System.out.print("por debajo del peso ideal"); break;
            case +0: System.out.print("en su rango de peso ideal"); break;
            case +1: System.out.print("por encima del peso ideal"); break;
        };
        System.out.println();
        System.out.print(" p2 está ");
        switch (p2.calcularIMC()) {
            case -1: System.out.print("por debajo del peso ideal"); break;
            case +0: System.out.print("en su rango de peso ideal"); break;
            case +1: System.out.print("por encima del peso ideal"); break;
        };
        System.out.println();
        System.out.print(" p3 está ");
        switch (p3.calcularIMC()) {
            case -1: System.out.print("por debajo del peso ideal"); break;
            case +0: System.out.print("en su rango de peso ideal"); break;
            case +1: System.out.print("por encima del peso ideal"); break;
        };
        System.out.println();
        
        System.out.println("----------------------------------------------------------------");
        
        System.out.println(" p1 es " + ((p1.esMayorDeEdad()) ? "mayor" : "menor") + " de edad.");
        System.out.println(" p2 es " + ((p2.esMayorDeEdad()) ? "mayor" : "menor") + " de edad.");
        System.out.println(" p3 es " + ((p3.esMayorDeEdad()) ? "mayor" : "menor") + " de edad.");
        
        
        System.out.println("----------------------------------------------------------------");
        
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
    }
    
}
